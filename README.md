A R T I S D A R K

Shader Keyword Tool
-------------------
What does this tool do?

Helps to deal with the Maximum number (128) of shader keywords exceeded’ problem. It scans for all shaders in the project, catalogs thier Keywords then it generates a convenient list of the shader with buttons to
navigate to the shader, including shader keyword counts and keywords info, so you can determine which ones to delete. Once you have deleted shaders you don't need you can then use the tool to remove all the
Keywords associated with the shaders from project materials.

How to use
----------

IMPORTANT back up your work, this tool will alter your materials and is cannot be undone! copy the ShaderKeywordsTool folder to your Assets folder

1. drag the ShaderKeywordsTool prefab from the ShaderKeywordsTool folder onto the stage
2. select it and click the green button 'Scan project for Shader Keywords’
3. You can click on the ‘Show Shader’ and it will take you to the shader in the project window
4. You can highlight multiple shaders to count the keywords used between shaders
6. Clicking the ‘More Info’ button will show the path to the shader and keywords found
7. Delete unused shaders.
8. Rescan project - 'Scan project for Shader Keywords’
9. Click the red button, tool will automatically search all materials and remove the keywords that are now longer used in the project, make sure you have backed up your project
10. In unity goto Edit/Project Settings/Graphics and click on the clear button at the bottom near ‘Currently tracked’
11. Restart project

I have used this on a big project with around 3500 Materials and found it really useful. I hope you find this tool useful also

Taz Cebula - Art Is Dark